import flickbike.importer
import gbfs.importer
import gbfs.donkey_importer
import gbfs.importer_oauth
import time
import psycopg2
import configparser
import os
from cycle_location import CycleLocation
import elasticapm

elasticapm.instrument()

config = configparser.ConfigParser()
config.read('config.ini')

apm_client = elasticapm.Client(service_name="vehicle_importer_deelfietsdashboard", server_url=os.getenv("APM_SERVER_URL"))
apm_client.capture_message('Billing process succeeded.')

importers = [
    flickbike.importer.FlickBikeImporter(config['flickbike']['url'])
    #gbfs.donkey_importer.DonkeyImporter('donkey', os.getenv("DONKEY_URL"), os.getenv("DONKEY_TOKEN")),
    # The only data source that is open data at this moment, ideal for testing purposes, thanks cykl!
    #gbfs.importer.GBFSFreeBikeStatusImporter('cykl', 'https://www.cykl.nl/gbfs/en/free_bike_status.json', {}),
    #gbfs.importer.GBFSFreeBikeStatusImporter('htm', 'https://eu-feeds.joyridecity.bike/api/v1/htm/htm_personenvervoer/gbfs/1/free_bike_status.json', {}),
    #gbfs.importer.GBFSFreeBikeStatusImporter('jump', os.getenv("JUMP_URL"), {'Authorization': "Bearer " + os.getenv("JUMP_TOKEN")}),
    #gbfs.importer.GBFSFreeBikeStatusImporter('gosharing', 'https://greenmo.core.gourban-mobility.com/api/gbfs/free-bike-status', {'Authorization-GBFS': "Bearer " + os.getenv("GOSHARING_TOKEN")}),
    #gbfs.importer.GBFSFreeBikeStatusImporter('felyx', 'https://data.felyx.com/gbfs/free_bike_status.json', {'x-api-key' : os.getenv("FELYX_TOKEN")}),
#    gbfs.importer.GBFSFreeBikeStatusImporter('deelfietsnederland', 'https://api.deelfietsnederland.nl/feeds/gbfs/free_bike_status.json', {'Authorization' : "Bearer " + os.getenv("DEELFIETSNEDERLAND_TOKEN")}),
    #gbfs.importer_oauth.GBFSFreeBikeStatusImporterOAuth('check', 'http://api.ridecheck.app/gbfs/free_bike_status.json',
    #    {'token_request': {'client_id': os.getenv("CHECK_CLIENT_ID"), 'client_secret': os.getenv("CHECK_CLIENT_SECRET"), 
    #        'audience': 'https://api.ridecheck.app', 'grant_type':'client_credentials'},
    #    'token_url': 'https://auth.ridecheck.app/oauth/token' 
    #    })
]

conn_str = "dbname=deelfietsdashboard"
if "dev" in os.environ:
    conn_str = "dbname=deelfietsdashboard3"

if "ip" in os.environ:
    conn_str += " host={} ".format(os.environ['ip'])
if "password" in os.environ:
    conn_str += " user=deelfietsdashboard password={}".format(os.environ['password'])


conn = psycopg2.connect(conn_str)
#cacher = cacher.cacher.Cacher(conn)
cur = conn.cursor()


class BikeImporter():
    def __init__(self):
        self.cycle_locations = {}

    def initialize(self):
        # Recover existing bikes into program.
        stmt = """SELECT DISTINCT(bike_id)
            FROM cycle_detection;
            """
        cur.execute(stmt)
        all_bikes = cur.fetchall()
        # Recover last location of bike.
        for bike_id in all_bikes:
            stmt2 = """SELECT bike_id, ST_Y(location), ST_X(location), system_id, extract(epoch from last_time_imported), is_check_in, is_check_out, sample_id
            FROM cycle_detection
            WHERE bike_id = %s
            ORDER BY sample_id DESC limit 2;
            """
            cur.execute(stmt2, (bike_id[0],))
            last_detection = cur.fetchone()
            detection = CycleLocation.parse_from_database(last_detection)
            
            second_last_detection = cur.fetchone()
            if not detection.c_in and second_last_detection:
                detection.corresponding_check_in = CycleLocation.parse_from_database(second_last_detection)
            self.cycle_locations[detection.bike_id] = detection



    # Check if bicycles are moved, or new in dataset.
    def check_movement(self, new_locations):
        detected_bicycles = set()
        for current_location in new_locations:
            detected_bicycles.add(current_location.bike_id)
            # Check if trip is made
            if current_location.bike_id in self.cycle_locations:
                self.check_trip(current_location)
            # New bike add, to set of cycle locations.
            else: 
                self.cycle_locations[current_location.bike_id] = current_location
                self.cycle_locations[current_location.bike_id].check_in(cur)
            
            self.cycle_locations[current_location.bike_id].update_timestamp()

        for bike_id in (set(self.cycle_locations.keys()) - detected_bicycles):
            self.cycle_locations[bike_id] = self.cycle_locations[bike_id].check_out(cur)
            if self.cycle_locations[bike_id].corresponding_check_in == None:
                print("\033[91m {}\033[00m" .format("Recover"))
                print(self.cycle_locations[bike_id])
                print(bike_id)

    def check_trip(self, current_location):
        previous_location = self.cycle_locations[current_location.bike_id]
        if previous_location == None:
            print("None type" + current_location.bike_id)

        # Check if bicycle has made trip.
        if previous_location.has_made_trip(current_location):
            # Check out previous location if not already checked out.
            previous_location = previous_location.check_out(cur)
            self.cycle_locations[current_location.bike_id] = current_location
            self.cycle_locations[current_location.bike_id].check_in(cur)
        else:
            # Recover false check_out
            if previous_location.c_out:
                self.cycle_locations[current_location.bike_id] = previous_location.recover(cur)
                print("Recover")
                print(current_location.bike_id)
                if self.cycle_locations[current_location.bike_id] == None:
                    print("\033[91m {}\033[00m" .format("Recover problem"))

    def import_loop(self):
        self.initialize()
        while True:
            start = time.time()

            detected_bicycles = []
            for importer in importers:
                transaction_id = 'import_feed' 
                try:
                    apm_client.begin_transaction(transaction_id)
                    detected_bicycles += importer.import_feed()
                    apm_client.end_transaction(importer.feed, 'success') 
                except Exception as e:
                    print(e)
                    apm_client.end_transaction(importer.feed, 'failure')
            self.check_movement(detected_bicycles)
            
            conn.commit()
            
            # Update materialized view
            cur.execute('REFRESH MATERIALIZED VIEW last_detection_cycle')
            conn.commit()
            
            time_to_sleep = 30 - (time.time() - start)
            print("Slaap: ", time_to_sleep)
            if time_to_sleep > 0.0:
                time.sleep(time_to_sleep)

bikeImporter = BikeImporter()
bikeImporter.import_loop()
