import requests
import cycle_location
import time


class GBFSFreeBikeStatusImporter():
    def __init__(self, feed, url, headers):
        self.feed = feed
        self.url = url
        self.headers = headers

    def import_bike(self, feed, bike, timestamp):
        return cycle_location.CycleLocation(feed + ':' + bike["bike_id"], bike["lat"], bike["lon"], feed, int(timestamp))

    def import_json(self, feed, json):
        cycles = []
        for bike in json["data"]["bikes"]:
            cycles.append(self.import_bike(feed, bike, json["last_updated"]))
        return cycles

    def import_feed(self):
        r = requests.get(self.url, headers=self.headers)
        if r.status_code == 200:
            try: 
                data = r.json()
                return self.import_json(self.feed, data)
            except Exception as e:
                print(e)
                print("Something went wrong.")
        return []
