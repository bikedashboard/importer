import requests
import cycle_location
import time


class DonkeyImporter():
    def __init__(self, feed, base_url, token):
        self.feed = feed
        self.base_url = base_url
        self.token = token
        self.headers = {'X-Api-Key': self.token}

    def import_bike(self, feed, bike, timestamp):
        return cycle_location.CycleLocation(feed + ':' + str(bike["bike_id"]), bike["lat"], bike["lon"], feed, int(timestamp))

    def import_json(self, feed, json):
        cycles = []
        for bike in json["data"]["bikes"]:
            cycles.append(self.import_bike(feed, bike, json["last_updated"]))
        return cycles

    def import_feed(self):
        r = requests.get(self.base_url + "/api/aggregators/cities?country_code=NL", headers=self.headers)
        data = None
        if r.status_code == 200:
            try: 
                data = r.json()
            except Exception as e:
                print(e)
                print("Something went wrong donkey importer.")
        
        result = []
        for city in data:
            result += self.import_gbfs_of_city(city["id"])
        return result

       
    def import_gbfs_of_city(self, city_id):
        r = requests.get(self.base_url + "/api/aggregators/bikes?city_id=" + str(city_id), headers=self.headers)
        if r.status_code == 200:
            try: 
                data = r.json()
                return self.import_json(self.feed, data)
            except Exception as e:
                print(e)
                print("Something went wrong import_gbfs_of_city donkey.")
        return []
