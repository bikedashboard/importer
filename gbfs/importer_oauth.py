import requests
import cycle_location
import time
import requests
import gbfs.importer


class GBFSFreeBikeStatusImporterOAuth():
    def __init__(self, feed, url, oauth_details):
        self.feed = feed
        self.url = url
        self.oauth_details = oauth_details
        self.free_bike_status_client = None
        self.expire_time = 0

    def refresh_token(self):
        print(self.oauth_details["token_request"])
        try:
            r = requests.post(self.oauth_details["token_url"], json=self.oauth_details["token_request"])
        except requests.exceptions.RequestException as e:
            print("Receiving new token went wrong.")

        print(r.status_code)
        print(r.text)
        if r.status_code == 200:
            data = r.json()
            print(data)
            self.free_bike_status_client = gbfs.importer.GBFSFreeBikeStatusImporter(self.feed, 
                self.url, {'Authorization': "Bearer " + data["access_token"]})
            self.expire_time = int(time.time()) + data["expires_in"] - 5
        else:
            self.free_bike_status_client = None

    def import_feed(self):
        if int(time.time()) >= self.expire_time:
            self.refresh_token()

        if self.free_bike_status_client:
            return self.free_bike_status_client.import_feed()
        else:
            print("Something is wrong with the check token.")
        return []
