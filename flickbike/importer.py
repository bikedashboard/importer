import requests
import cycle_location
import time

class FlickBikeImporter():
    def __init__(self, url):
        self.feed = 'flickbike'
        self.url = url

    def import_bike(self, feed, bike):
        return cycle_location.CycleLocation(feed + ':' + bike["bid"], bike["gLat"], bike["gLng"], feed, time.time())

    def import_json(self, feed, json):
        cycles = []
        for bike in json["data"]:
            cycles.append(self.import_bike(feed, bike))
        return cycles

    def import_feed(self):
        try: 
            r = requests.get(self.url)
            if r.status_code == 200:
                data = r.json()
                return self.import_json("flickbike", data)
        except:
            print("Something went wrong.")
        return []
