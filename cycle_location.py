from geopy.distance import distance
import time

class CycleLocation:
    def __init__(self, bike_id, latitude, longitude, system_id, last_time_updated):
        self.bike_id = bike_id
        self.latitude = latitude 
        self.longitude = longitude
        self.system_id = system_id
        self.c_in = False
        self.c_out = False
        self.corresponding_check_in = None
        self.last_time_updated = last_time_updated

    def save(self, cur):
        cur.execute("""INSERT INTO cycle_detection 
            (bike_id, location, system_id, is_check_in, is_check_out, last_time_imported) 
            VALUES (%s, ST_SetSRID(ST_Point(%s, %s),4326), %s, %s, %s, now()) RETURNING sample_id;""" 
            , (self.bike_id, self.longitude, self.latitude, self.system_id, self.c_in, self.c_out))
        self.sample_id = cur.fetchone()[0]
        return self

    def distance(self, other):
        return distance((self.latitude, self.longitude), (other.latitude, other.longitude)).m 

    def has_significant_delta_location(self, other):
        return self.distance(other) > 200

    def is_used_and_parked_on_same_location(self, new):
        return (new.last_time_updated - self.last_time_updated) > 15*60 and not self.is_on_same_location(new)

    def is_on_same_location(self, other):
        return self.distance(other) < 0.01

    def has_made_trip(self, new):
        if self.has_significant_delta_location(new):
            return True
        if self.is_used_and_parked_on_same_location(new):
            return True
        return False

    def check_in(self, cur):
        self.c_in = True
        self.save(cur)
    
    def check_out(self, cur):
        if not self.c_out:
            print("Check_out ", self.bike_id)
            checkout = CycleLocation(self.bike_id, self.latitude, self.longitude, self.system_id, self.last_time_updated)
            checkout.c_out = True
            checkout.save(cur)
            checkout.corresponding_check_in = self
            return checkout
        return self

    # Recovers last check_out because it was incorrect.
    def recover(self, cur):
        if not self.c_out:
            print("\033[91m {}\033[00m" .format("Should not be called"))
        cur.execute("""DELETE FROM cycle_detection 
            WHERE sample_id = %s""", (self.sample_id,))
        stmt = """INSERT INTO
            recover_park_events
            VALUES (%s, %s, %s, %s)
            """
        cur.execute(stmt, (self.system_id, self.bike_id, self.sample_id, self.corresponding_check_in.sample_id))
        return self.corresponding_check_in


    def reset(self):
        self.c_in = False
        self.c_out = False

    def update_timestamp(self):
        self.last_time_updated = time.time()

    # Parse record in to object.
    @staticmethod
    def parse_from_database(record):
        # Init with current time to avoid trip detection due to slight changes.
        detection = CycleLocation(record[0], record[1], record[2], record[3], time.time())
        detection.c_in = record[5]
        detection.c_out = record[6]
        detection.sample_id = record[7] 
        return detection

